# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import work
from . import configuration
from . import invoice
from . import sale
from . import purchase
from . import production
from . import stock


def register():
    Pool.register(
        configuration.Configuration,
        configuration.ConfigurationCompany,
        work.Project,
        work.WorkProjectCreateValuedRelationStart,
        sale.Sale,
        sale.SaleLine,
        sale.SaleAwardStart,
        stock.ShipmentOut,
        invoice.Invoice,
        invoice.InvoiceLine,
        purchase.Purchase,
        purchase.PurchaseRequisition,
        purchase.PurchaseRequisitionLine,
        purchase.PurchaseRequest,
        production.Production,
        production.ProductionProjectCalendar,
        module='electrans_work_project', type_='model')
    Pool.register(
        sale.SaleAward,
        purchase.CreatePurchase,
        sale.CreateOrder,
        work.WorkProjectCreateValuedRelation,
        module='electrans_work_project', type_='wizard')
