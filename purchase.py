# -*- encoding: utf-8 -*-
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    project = fields.Many2One('work.project', 'Project')


class PurchaseRequisition(metaclass=PoolMeta):
    __name__ = 'purchase.requisition'

    project = fields.Many2One('work.project', 'Project')


class PurchaseRequest(metaclass=PoolMeta):
    __name__ = 'purchase.request'

    project = fields.Many2One('work.project', 'Project')


class PurchaseRequisitionLine(metaclass=PoolMeta):
    __name__ = 'purchase.requisition.line'

    def compute_request(self):
        request = super(PurchaseRequisitionLine, self).compute_request()
        if request:
            request.project = self.requisition.project
            request.save()
            return request


class CreatePurchase(metaclass=PoolMeta):
    __name__ = 'purchase.request.create_purchase'

    def transition_start(self):
        res = super(CreatePurchase, self).transition_start()
        if res == 'ask_party':
            return res
        pool = Pool()
        Request = pool.get('purchase.request')
        RequisitionLine = pool.get('purchase.requisition.line')
        Production = pool.get('production')
        requests = Request.browse(Transaction().context['active_ids'])
        for request in requests:
            if isinstance(request.origin, RequisitionLine) or isinstance(request.origin, Production):
                request.purchase.project = request.project
                request.purchase.save()
        return 'create_'
