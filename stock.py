# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class ShipmentOut(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    project = fields.Function(
        fields.Many2One('work.project', "Project"),
        'get_project')

    def get_project(self, name=None):
        project = None
        for sale in self.sales:
            if sale.project:
                project = sale.project.id
                break

        return project


