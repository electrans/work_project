# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelView, fields, ModelSQL
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.transaction import Transaction
from sql import Literal, Null
from sql.aggregate import Count, Min
from sql.functions import CurrentTimestamp
from sql.operators import Concat, Not


class Production(metaclass=PoolMeta):
    __name__ = 'production'
    work_project = fields.Many2One('work.project', 'Work Project',
                                   states={'readonly': ~Eval('state').in_(
                                       ['request', 'draft']), },
                                   depends=['state']
                                   )

    @classmethod
    @ModelView.button
    def create_purchase_request(cls, productions):
        Request = Pool().get('purchase.request')
        requests = []
        super(Production, cls).create_purchase_request(productions)
        for production in productions:
            purchase_request = production.purchase_request
            if purchase_request:
                purchase_request.project = production.work_project
                requests.append(purchase_request)
        if requests:
            Request.save(requests)


class ProductionProjectCalendar(ModelSQL, ModelView):
    """Production Project Calendar"""
    __name__ = 'production.project.calendar'
    project = fields.Many2One('work.project', 'Project', readonly=True)
    count = fields.Char('Productions Quantity', readonly=True)
    planned_date = fields.Date('Planned Date', readonly=True)
    productions = fields.Function(fields.One2Many('production',
                                                  None, 'Productions'),
                                  'get_productions')
    calendar_color = fields.Function(fields.Char('Color'),
                                     'get_calendar_color')

    def get_calendar_color(self, name):
        for production in self.productions:
            for shipment in production.supply_shipments:
                if shipment.state == 'done':
                    return 'orange'

    @classmethod
    def table_query(cls):
        pool = Pool()
        Production = pool.get('production')
        production = Production.__table__()
        context = Transaction().context
        # to filter two different windows between internal and external
        # productions. If a production has purchase request is an external one,
        # if it doesn't have it, is an internal production
        # Make the clause to filter the records using context variable
        if context.get('external_productions'):
            production_origin_clause = production.purchase_request != Null
        else:
            production_origin_clause = production.purchase_request == Null
        query = production.select(
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            Literal(None).as_('write_uid'),
            Literal(None).as_('write_date'),
            Min(production.id).as_('id'),
            production.work_project.as_('project'),
            production.planned_date.as_('planned_date'),
            Concat('Cantidad Producciones: ',
                   Count(Literal('*'))).as_('count'),
            where=(production.work_project != Null) & production_origin_clause
                  & (Not(production.state.in_(['cancelled', 'done']))),
            group_by=[production.work_project, production.planned_date]
        )
        return query

    def get_productions(self, name):
        pool = Pool()
        Production = pool.get('production')
        context = Transaction().context
        if context.get('external_productions'):
            clause = ('purchase_request', '!=', None)
        else:
            clause = ('purchase_request', '=', None)
        productions = Production.search([
            ('work_project', '=', self.project),
            ('planned_date', '=', self.planned_date),
            ('state', 'not in', ['cancelled', 'done']),
            clause])
        return [p.id for p in productions]
