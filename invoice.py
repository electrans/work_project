# -*- encoding: utf-8 -*-
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Bool, If

__all__ = ['InvoiceLine']


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    work_project = fields.Many2One(
        'work.project', "Work Project",
        states={
            'invisible': ~Eval('_parent_invoice', {}).get('type', Eval(
                'invoice_type')).in_(['in_invoice', 'in_credit_note'])})
    project_moves = fields.Function(fields.One2Many('stock.move', None,
        "Project Moves"), 'get_project_moves')

    @classmethod
    def __setup__(cls):
        super(InvoiceLine, cls).__setup__()
        # The domain will be applied only when the invoice type is customer
        # and it have work project linked
        cls.stock_moves.depends.append('project_moves')
        cls.stock_moves.domain = [If(
            Bool(Eval('project_moves', [])),
            ('id', 'in', Eval('project_moves', [])),
            ()
        )]

    def get_project_moves(self, name):
        pool = Pool()
        Move = pool.get('stock.move')
        if self.invoice and self.invoice.type == 'out'\
                and self.invoice.work_project:
            return [m.id for m in Move.search([
                ('sale', 'in', self.invoice.work_project.sales)])]
        return []


class Invoice(metaclass=PoolMeta):
    __name__ = 'account.invoice'

    work_project = fields.Many2One(
        'work.project', 'Project',
        domain=[('party', '=', Eval('party'))],
        states={'invisible': Eval('type') != 'out'},
        depends=['party', 'type'])
    project_category = fields.Function(fields.Many2One('sale.category',
        'Project Category'), 'get_project_category')
    project_description = fields.Text('Project Description', states={
            'readonly': Eval('state') != 'draft',
            },
        depends=['state'],
        help="It will be printed only in the first page, "
        "over the column titles.")
    is_valued_relation = fields.Function(
        fields.Boolean("Is Valued Relation"),
        'get_is_valued_relation',
        searcher='search_is_valued_relation')
    # Field that helps to define if an invoice is a valued_relation
    is_invoice_valued_relation = fields.Boolean("Is Valued Relation")

    def get_project_category(self, name):
        return self.work_project.category.id if self.work_project and\
            self.work_project.category else None

    def _credit(self, **values):
        credit = super(Invoice, self)._credit(**values)
        for field in [
                'project_description', 'work_project', 'project_category']:
            setattr(credit, field, getattr(self, field))
        return credit

    def get_is_valued_relation(self, name=None):
        if self.is_invoice_valued_relation and self.state == 'draft':
            return True

    @classmethod
    def search_is_valued_relation(cls, name, clause):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        condition = 'in' if clause[2] else 'not in'
        valued_relations = Invoice.search([('is_invoice_valued_relation', '=', True),
                                           ('state', '=', 'draft')
                                           ])
        p_ids = [v.id for v in valued_relations]
        return [('id', condition, p_ids)]
