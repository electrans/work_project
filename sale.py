# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from trytond.model import ModelView, fields
from trytond.pyson import Eval, If, Bool
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.wizard import Wizard, Button, StateView, StateTransition
from trytond.i18n import gettext
from trytond.exceptions import UserError


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'
    project = fields.Many2One(
        'work.project', "Project",
        domain=[If(
                Bool(Eval('category', False)),
                [('party', '=', Eval('party')), ('category', '!=', None)],
                ['party', '=', Eval('party')])],
        states={
            'readonly': ~Eval('state').in_(['draft', 'quotation', 'confirmed']),
            'required': Eval('state').in_(['processing', 'done'])},
        depends=['party', 'state', 'category'])

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        cls.invoice_method.selection.extend([('valued_relation', 'Valued Relation')])

    def get_rec_name(self, name):
        rec_name = super(Sale, self).get_rec_name(name)
        if Transaction().context.get('hide_reference', False) and self.reference:
            rec_name = rec_name.replace(" [" + self.reference + "]", "")
        return rec_name

    def invoiced_amount(self):
        amount = Decimal('0.00')
        for invoice in self.invoices:
            amount += invoice.untaxed_amount
        return amount

    def amount_to_invoice(self):
        return self.untaxed_amount - self.invoiced_amount()

    def create_invoice(self):
        context = Transaction().context
        # When invoice_method is valued_relation, the invoice will have to be created using the wizard
        if ((context.get('valued_relation') and self.invoice_method == 'valued_relation') or
                self.invoice_method != 'valued_relation'):
            invoice = super(Sale, self).create_invoice()
            if invoice:
                invoice.work_project = self.project
                invoice.project_category = invoice.work_project.category.id if invoice.work_project and invoice.work_project.category else None
                if self.invoice_method == 'valued_relation':
                    # Used at the getter of is_valued_relation field to define if it's a valued_relation
                    invoice.is_invoice_valued_relation = True
                invoice.save()
            return invoice
        return

    def check_method(self):
        '''
        Check the methods.
        '''
        super(Sale, self).check_method()
        if self.shipment_method == 'invoice' and self.invoice_method == 'valued_relation':
            raise SaleValidationError(
                gettext('sale.msg_sale_invalid_method',
                    invoice_method=self.invoice_method_string,
                    shipment_method=self.shipment_method_string,
                    sale=self.rec_name))

    @classmethod
    def copy(cls, sales, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default['project'] = None
        return super(Sale, cls).copy(sales, default=default)


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    def _get_invoice_not_line(self):
        # It will enter into this condition, when the valued_relation wizard is executed, cause if not the main
        # create_invoice() method won't be called that is the function which this function is called from
        return (self.sale.invoice_method == 'valued_relation' or
                super(SaleLine, self)._get_invoice_not_line())

    def _get_invoice_line_quantity(self):
        # It will enter into this condition, when the valued_relation wizard is executed, cause if not the main
        # create_invoice() method won't be called that is the function which this function is called from
        if self.sale.invoice_method == 'valued_relation':
            # If line quantity is 0, the invoice won't be created, so we return 1, cause core will do:
            # (self._get_invoice_line_quantity() - self._get_invoiced_quantity()) to get the qty, so we will get qty=
            # later in the wizard the quantities will be set to 0
            return 1.0
        else:
            return super(SaleLine, self)._get_invoice_line_quantity()

    def _get_invoiced_quantity(self):
        # It will enter into this condition, when the valued_relation wizard is executed, cause if not the main
        # create_invoice() method won't be called that is the function which this function is called from
        if self.sale.invoice_method == 'valued_relation':
            # If line quantity is 0, the invoice won't be created, so we return 0, cause core will do:
            # (self._get_invoice_line_quantity() - self._get_invoiced_quantity()) to get the qty, so we will get qty=1
            # later in the wizard the quantities will be set to 0
            return 0.0
        else:
            return super(SaleLine, self)._get_invoiced_quantity()


class SaleAwardStart(ModelView):
    'Sale Award Start'
    __name__ = 'sale.award.start'

    number = fields.Char(
        'Number',
        required=True)
    title = fields.Char(
        'Title')
    area_manager = fields.Many2One(
        'company.employee', 'Area Manager')
    delivery_date = fields.Date('Delivery Date')
    category = fields.Many2One('sale.category', 'Category')
    country = fields.Many2One('country.country', 'Country')


class SaleAward(Wizard):
    'Sale Award'
    __name__ = 'sale.award'
    start = StateView('sale.award.start',
                      'electrans_work_project.sale_award_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Award', 'award', 'tryton-ok', default=True),
                      ])
    award = StateTransition()

    def default_start(self, fields):
        Sale = Pool().get('sale.sale')
        default = {}
        sale = Sale(Transaction().context['active_id'])
        if sale.project:
            raise UserError(gettext(
                'electrans_work_project.project_already_defined'))
        if sale.state != 'confirmed':
            raise UserError(gettext(
                'electrans_work_project.sale_not_in_confirmed'))
        default['number'] = sale.number
        default['title'] = sale.description
        default['category'] = sale.category.id if sale.category else None
        default['country'] = sale.country.id if sale.country else None
        return default

    def transition_award(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        Project = pool.get('work.project')
        sale = Sale(Transaction().context['active_id'])
        active_ids = Transaction().context['active_ids']
        for _id in active_ids:
            sale = Sale(_id)
            if sale.origin and isinstance(sale.origin, Sale):
                quotations = [sale.origin.id]
        Project.create([{
            'number': self.start.number,
            'party': sale.party,
            'title': self.start.title,
            'area_manager': self.start.area_manager,
            'category': self.start.category.id if self.start and self.start.category else None,
            'country': self.start.country.id if self.start and self.start.country else None,
            'delivery_date': self.start.delivery_date,
            'state': 'draft',
            'sales': [('add', active_ids)],
            'quotations': [('add', quotations)]}])
        return 'end'


class CreateOrder(metaclass=PoolMeta):
    __name__ = 'sale.create_order'

    def copy_sale(self):
        # Adds project field to duplicated sale record
        sale = super(CreateOrder, self).copy_sale()
        sale.project = self.record.project
        sale.save()
        return sale
