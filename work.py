# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
import datetime
from sql.aggregate import Sum, Count
from sql.conditionals import Case
from trytond.model import ModelSQL, ModelView, fields, Workflow, Unique
from trytond.wizard import Wizard, StateView, Button, StateAction
from trytond.pyson import Eval, If, Bool
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from trytond.i18n import gettext
from trytond.exceptions import UserError, UserWarning

__all__ = ['Project']

__metaclass__ = PoolMeta

_ZERO = Decimal('0.0')

_STATES = {'readonly': Eval('state') == 'closed'}
_DEPENDS = ['state']


class Project(Workflow, ModelSQL, ModelView):
    "Work Project"
    __name__ = 'work.project'

    company = fields.Many2One(
        'company.company', "Company",
        required=True,
        select=True,
        states=_STATES,
        depends=_DEPENDS,
        domain=[
            ('id',
             If(Eval('context', {}).contains('company'), '=', '!='),
             Eval('context', {}).get('company', -1))])
    currency_digits = fields.Function(
        fields.Integer(
            "Currency Digits"),
        'on_change_with_currency_digits')
    number = fields.Char(
        "Number",
        required=True,
        select=True,
        states={
            'readonly': Eval('code_readonly', True) | Bool(Eval('state') == 'closed')},
        depends=['code_readonly', 'state'])
    code_readonly = fields.Function(
        fields.Boolean("Code Readonly"),
        'get_code_readonly')
    party = fields.Many2One(
        'party.party', "Party",
        states={
            'readonly': Eval('state') == 'closed',
            'required': True},
        depends=_DEPENDS)
    start_date = fields.Date(
        "Start Date")
    end_date = fields.Date(
        "End Date",
        readonly=True)
    category = fields.Many2One('sale.category', 'Category')
    sales = fields.One2Many(
        'sale.sale', 'project', "Sales",
        states=_STATES,
        filter=[('is_order', '=', True)],
        add_remove=[('state', 'in', ['quotation', 'confirmed', 'processing'])],
        depends=['state'])
    purchases = fields.One2Many('purchase.purchase', 'project', "Purchases", states = {
        'readonly': True}, depends = ['state'])
    quotations = fields.One2Many(
        'sale.sale', 'project', "Quotations",
        states=_STATES,
        filter=[('is_order', '=', False)],
        add_remove=[('state', 'in', ['quotation', 'confirmed', 'processing'])],
        depends=['state'])
    lines = fields.Function(
        fields.One2Many(
            'sale.line', None, "Sale Lines"),
        'get_sale_lines')
    supplier_invoice_lines = fields.One2Many(
        'account.invoice.line', 'work_project', "Supplier Invoice Lines",
        domain=[('invoice.type', 'in', ['in_invoice', 'in_credit_note'])],
        states=_STATES,
        depends=_DEPENDS)
    amount_to_invoice = fields.Function(
        fields.Numeric(
            "Amount To Invoice",
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'get_amount_to_invoice')
    invoiced_amount = fields.Function(
        fields.Numeric(
            "Invoiced Amount",
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'get_amount_to_invoice')
    shipments = fields.Function(
        fields.One2Many(
            'stock.shipment.out', None, "Shipments"),
        'get_shipments')
    shipment_returns = fields.Function(
        fields.One2Many(
            'stock.shipment.out.return', None, "Shipment Returns"),
        'get_shipment_returns')
    internal_shipments = fields.Function(
        fields.One2Many(
            'stock.shipment.internal', None, "Internal Shipments",
            states={
                'invisible': ~Eval('internal_shipments'),
            }),
        'get_internal_shipments')
    moves = fields.Function(
        fields.One2Many(
            'stock.move', None, "Moves"),
        'get_moves')
    income_labor = fields.Function(
        fields.Numeric(
            "Income Labor",
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'get_income_labor')
    income_material = fields.Function(
        fields.Numeric(
            "Income Material",
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'get_income_material')
    income_other = fields.Function(
        fields.Numeric(
            "Income Other",
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'get_income_other')
    expense_material = fields.Function(
        fields.Numeric(
            "Expense Material",
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'get_expense_material')
    expense_other = fields.Function(
        fields.Numeric(
            "Expense Other",
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'get_expense_other')
    margin_labor = fields.Function(
        fields.Numeric(
            "Margin Labor",
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'get_margins')
    margin_material = fields.Function(
        fields.Numeric(
            "Margin Material",
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'get_margins')
    margin_other = fields.Function(
        fields.Numeric(
            "Margin Other",
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'get_margins')
    margin_percent_labor = fields.Function(
        fields.Numeric(
            "Margin(%) Labor",
            digits=(16, 4)),
        'get_margins')
    margin_percent_material = fields.Function(
        fields.Numeric(
            "Margin (%) Material",
            digits=(16, 4)),
        'get_margins')
    margin_percent_other = fields.Function(
        fields.Numeric(
            "Margin (%) Other",
            digits=(16, 4)),
        'get_margins')
    note = fields.Text(
        "Note",
        states=_STATES,
        depends=_DEPENDS)
    invoices = fields.Function(
        fields.One2Many(
            'account.invoice', None, "Invoices"),
        'get_invoices')
    shipment_state = fields.Function(
        fields.Selection([
            ('exception', 'Exception'),
            ('sent', 'Sent'),
            ('partially sent', 'Partially sent'),
            ('waiting', 'Waiting'),
            ('none', 'None')
            ], "Shipment State"),
        'get_shipment_state',
        searcher='search_shipment_state_field')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('planning', 'Planning'),
        ('executing', 'Executing'),
        ('closed', 'Closed')
        ], "State",
        readonly=True,
        select=True)
    title = fields.Char(
        "Title",
        states=_STATES,
        depends=_DEPENDS)
    project_manager = fields.Many2One(
        'company.employee', "Project Manager",
        states=_STATES,
        depends=_DEPENDS)
    area_manager = fields.Many2One(
        'company.employee', "Area Manager",
        states=_STATES,
        depends=_DEPENDS)
    delivery_date = fields.Date('Delivery Date', readonly=True)
    country = fields.Many2One('country.country', 'Country')
    productions = fields.One2Many('production', 'work_project',
                                  "Productions", readonly=True)
    total_labor_time = fields.Function(
        fields.TimeDelta("Total Labor Time"),
        'get_total_labor_time')
    valued_relations = fields.Function(
        fields.One2Many(
            'account.invoice', None, "Valued Relations",
            states={
                'invisible': ~Eval('valued_relations'),
            }),
        'get_valued_relations')

    def get_total_labor_time(self, name=None):
        # Compute total production time dedicated to a project
        total_labor_time = sum(
            production.total_labor_time.total_seconds()
            for production in self.productions)
        return datetime.timedelta(seconds=total_labor_time)

    def get_rec_name(self, name=None):
        rec_name = self.number
        if self.project_manager:
            rec_name += f' ({self.project_manager.get_rec_name(None)})'
        return rec_name

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
                ('number',) + tuple(clause[1:]),
                ('project_manager',) + tuple(clause[1:]),
                ]

    @classmethod
    def __register__(cls, module_name):
        table = cls.__table_handler__(module_name)
        if table.column_exist('code'):
            table.column_rename('code', 'number')
        if table.column_exist('maintenance'):
            table.drop_column('maintenance')
        super(Project, cls).__register__(module_name)

    @classmethod
    def __setup__(cls):
        super(Project, cls).__setup__()
        p = cls.__table__()
        cls._transitions = set((
                ('draft', 'planning'),
                ('planning', 'draft'),
                ('planning', 'executing'),
                ('executing', 'closed'),
                ))
        cls._buttons.update({
                'draft': {
                    'invisible': Eval('state') != 'planning',
                    'icon': 'tryton-back',
                    'depends': ['state'],
                    },
                'plan': {
                    'invisible': Eval('state') != 'draft',
                    'icon': 'tryton-forward',
                    'depends': ['state'],
                    },
                'execute': {
                    'invisible': Eval('state') != 'planning',
                    'icon': 'tryton-forward',
                    'depends': ['state'],
                    },
                'close': {
                    'invisible': Eval('state') != 'executing',
                    'icon': 'tryton-forward',
                    'depends': ['state'],
                    },
                })
        cls._sql_constraints += [
            ('project_number_uniq', Unique(p, p.number),
                'electrans_work_project.msg_work_project_project_number_unique'),
            ]

    @classmethod
    def write(cls, *args):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        actions = iter(args)
        for records, values in zip(actions, actions):
            if 'type' in values:
                invoices = Invoice.search([
                    ('work_project', 'in', [r.id for r in records])])
                if invoices:
                    for invoice in invoices:
                        if invoice.state != 'draft':
                            raise UserError(
                                gettext('electrans_work_project.invoice_not_in_draft'))
                    Invoice.write(invoices, {'project_category': values['category']})
        super(Project, cls).write(*args)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, projects):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('planning')
    def plan(cls, projects):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('executing')
    def execute(cls, projects):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('closed')
    def close(cls, projects):
        Date = Pool().get('ir.date')
        for project in projects:
            if project.shipment_state not in ['sent', 'none']:
                raise UserError(gettext('electrans_work_project.pending_moves'))
            project.end_date = Date.today()
        cls.save(projects)

    @staticmethod
    def default_currency_digits():
        Company = Pool().get('company.company')
        if Transaction().context.get('company'):
            company = Company(Transaction().context['company'])
            return company.currency.digits
        return 2

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @fields.depends('company')
    def on_change_with_currency_digits(self, name=None):
        if self.company:
            return self.company.currency.digits
        return 2

    def is_labour_line(self, line):
        'Returns True if the sale line is of labour type'
        return line.product and line.product.type == 'service'

    def is_other_line(self, line):
        'Returns True if the sale line is of other type'
        return not line.product

    def get_income_labor(self, name):
        amount = _ZERO
        for sale in self.sales:
            for line in sale.lines:
                if not self.is_other_line(line) and self.is_labour_line(line):
                    amount += line.amount
        return amount

    def get_income_material(self, name):
        amount = _ZERO
        for sale in self.sales:
            for line in sale.lines:
                if (not self.is_other_line(line) and
                        not self.is_labour_line(line)):
                    amount += line.amount
        return amount

    def get_income_other(self, name):
        amount = _ZERO
        for sale in self.sales:
            for line in sale.lines:
                if self.is_other_line(line):
                    amount += line.amount
        return amount

    def get_expense_material(self, name):
        amount = _ZERO
        for sale in self.sales:
            for line in sale.lines:
                if line.product and line.product.type != 'service':
                    qty = Decimal(str(line.quantity or 0))
                    # Compatibility with sale_margin
                    if hasattr(line, 'cost_price'):
                        cost_price = line.cost_price or _ZERO
                    else:
                        cost_price = line.product.cost_price
                    amount += sale.currency.round(cost_price * qty)
        return amount

    def get_expense_other(self, name):
        amount = _ZERO
        return amount

    @classmethod
    def get_margins(cls, projects, names):
        res = {}
        for name in names:
            res[name] = dict((p.id, _ZERO) for p in projects)
        for project in projects:
            for name in names:
                field_name = name.split('_')[-1]
                income = getattr(project, 'income_%s' % field_name)
                expense = getattr(project, 'expense_%s' % field_name)
                if 'percent' in name:
                    if not expense:
                        value = Decimal('1.0')
                    else:
                        value = (income - expense) / expense
                    digits = getattr(cls, name).digits[1]
                else:
                    value = income - expense
                    digits = project.currency_digits
                value = value.quantize(Decimal(str(10 ** - digits)))
                res[name][project.id] = value
        return res

    @classmethod
    def get_amount_to_invoice(cls, projects, names):
        res = {}
        for name in names:
            res[name] = dict((p.id, _ZERO) for p in projects)

        for project in projects:
            amount_to_invoice = Decimal('0.00')
            invoiced_amount = Decimal('0.00')
            for sale in project.sales:
                amount_to_invoice += sale.amount_to_invoice()
                invoiced_amount += sale.invoiced_amount()
            res['amount_to_invoice'][project.id] = amount_to_invoice
            res['invoiced_amount'][project.id] = invoiced_amount
        return res

    def get_code_readonly(self, name):
        return True

    def get_moves(self, name):
        return [m.id for s in self.sales for m in s.moves]

    def get_shipments(self, name):
        return [m.id for s in self.sales for m in s.shipments]

    def get_shipment_returns(self, name):
        return [m.id for s in self.sales for m in s.shipment_returns]

    def get_internal_shipments(self, name):
        return [m.id for s in self.sales for m in s.internal_shipments]

    def get_invoices(self, name):
        return [m.id for s in self.sales for m in s.invoices]

    @classmethod
    def create(cls, vlist):
        'Fill the reference field with the sale sequence'
        pool = Pool()
        Config = pool.get('work.project.configuration')

        config = Config(1)
        for value in vlist:
            if not value.get('number'):
                if config.project_sequence:
                    number = config.project_sequence.get()
                else:
                    number = None
                value['number'] = number
        return super(Project, cls).create(vlist)

    @classmethod
    def copy(cls, projects, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['number'] = None
        return super(Project, cls).copy(projects, default=default)

    @classmethod
    def get_shipment_state(cls, records, _):
        '''
        Return the shipment state for the sale.
        '''
        cursor = Transaction().connection.cursor()
        pool = Pool()
        sale_table = pool.get('sale.sale').__table__()
        project_table = pool.get('work.project').__table__()
        cursor.execute(*project_table
            .join(sale_table, type_='LEFT',
                condition=(project_table.id == sale_table.project))
            .select(
                project_table.id,
                Sum(Case((sale_table.shipment_state == 'exception', 1), else_=0)).as_('exceptions'),
                Sum(Case((sale_table.shipment_state == 'sent', 1), else_=0)).as_('sent'),
                Count(sale_table.project).as_('count'),
                group_by=project_table.id))
        rs = cursor.fetchall()
        res = {f[0]:
            'none' if f[3]==0 else
            'exception' if f[1]>0 else
            'sent' if f[2]==f[3] else
            'partially sent' if f[2]>0 else
            'waiting' if f[3]>0 else
            'none'
            for f in rs if f[0]}
        return res

    @classmethod
    def search_shipment_state_field(cls, name, clause):
        shipment_state_filter = clause[2]
        projects = cls.search([])
        projects_shipment_state_dict = Project.get_shipment_state(projects, name)
        filtered_projects = [p.id for p in projects if projects_shipment_state_dict[p.id] == shipment_state_filter]
        if clause[1] == '=':
            return ['id', 'in', filtered_projects]
        elif clause[1] == '!=':
            return ['id', 'not in', filtered_projects]
        else:
            return None

    def get_sale_lines(self, name):
        SaleLine = Pool().get('sale.line')
        sales = [sale.id for sale in self.sales]
        lines = [line.id for line in SaleLine.search([('sale', 'in', sales), ('parent', '=', None)])]
        return lines

    def get_valued_relations(self, name):
        valued_relations = [i.id for sale in self.sales for i in sale.invoices if i.is_valued_relation]
        return valued_relations


class WorkProjectCreateValuedRelationStart(ModelView):
    'Work Project Create Valued Relation Start'
    __name__ = 'work.project.create.valued_relation.start'
    sales = fields.Many2Many(
        'sale.sale', None, None, 'Sales',
        # TODO: Domain state should only be in processing. See APP-5655
        domain=[('state', 'in', ['processing', 'done']),
                ('invoice_method', '=', 'valued_relation'),
                ('is_order', '=', True),
                ('id', 'in', Eval('domain_sales'))],
        depends=['domain_sales'],
    )
    domain_sales = fields.Many2Many(
        'sale.sale', None, None, 'Domain Sales')


class WorkProjectCreateValuedRelation(Wizard):
    'Work Project Create Valued Relation'
    __name__ = 'work.project.create.valued_relation'
    start = StateView('work.project.create.valued_relation.start',
                      'electrans_work_project.create_valued_relation_start_view_form', [
                          Button('Cancel', 'end', 'tryton-cancel'),
                          Button('Create', 'create_', 'tryton-ok', default=True),
                      ])
    create_ = StateAction('electrans_work_project.act_valued_relation_form')

    def default_start(self, fields):
        sales = []
        for project in self.records:
            sales = [sale.id for sale in project.sales]
        return {
            'sales': [],
            'domain_sales': sales,
            }

    def do_create_(self, action):
        pool = Pool()
        Warning = pool.get('res.user.warning')
        valued_relations = []
        # Set context cause when is not set (wizard not executed) create_invoice() won't create an invoice
        with Transaction().set_context(valued_relation=True):
            for sale in self.start.sales:
                if sale.invoices:
                    # If there is a valued_relation created, show warning
                    if any(i.is_valued_relation for i in sale.invoices):
                        key = 'existing_valued_relation_in_%s' % sale.id
                        if Warning.check(key):
                            raise UserWarning(key, gettext(
                                'electrans_work_project.existing_valued_relation',
                                sale=sale.number))
                # Create new Valued Relation
                valued_relation = sale.create_invoice()
                if valued_relation:
                    for line in valued_relation.lines:
                        if line.type == 'line':
                            line.quantity = 0
                        line.position = line.origin.position if line.origin else None
                        # Save before each line to update_taxes with the current valued relation
                        line.save()
                        valued_relation.update_taxes()
                    valued_relations.append(valued_relation.id)
        data = {'res_id': valued_relations}
        # If only 1 duplicated show record with form, else show them in tree
        if len(valued_relations) == 1:
            action['views'].reverse()
        return action, data
