# This file is part product_manufacturer module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
try:
    from trytond.modules.electrans_work_project.tests.test_work_project import suite
except ImportError:
    from .test_work_project import suite

__all__ = ['suite']
