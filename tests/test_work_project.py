# copyright notices and license terms.
import unittest
import trytond.tests.test_tryton
from decimal import Decimal
from trytond.modules.company.tests import create_company, set_company
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.modules.electrans_tools.tests.test_tools import (
    create_fiscalyear_and_chart, get_accounts, create_production, create_account_category,
    create_product, _create_bom)
import datetime


class WorkProjectTestCase(ModuleTestCase):
    """Test electrans work project module"""
    module = 'electrans_work_project'

    @with_transaction()
    def test_fields_dragging_to_purchase_request(self):
        "Test Modify To Location Internal Shipment"
        pool = Pool()
        Production = pool.get('production')
        Location = pool.get('stock.location')
        PurchaseRequest = pool.get('purchase.request')
        PurchaseRequisition = pool.get('purchase.requisition')
        PurchaseRequisitionLine = pool.get('purchase.requisition.line')
        WorkProject = pool.get('work.project')
        Tax = pool.get('account.tax')
        ProductUom = pool.get('product.uom')
        Party = pool.get('party.party')
        Employee = pool.get('company.employee')
        unit, = ProductUom.search([('name', '=', 'Unit')])
        company = create_company()
        with set_company(company):
            # Create party
            party = Party(name='Supplier 1')
            # Create employee
            employee = Employee(party=party)
            employee.save()
            # Create all necessary locations
            production_loc, = Location.search([('code', '=', 'PROD')])
            storage_location, = Location.search([('code', '=', 'STO')])
            warehouse, = Location.search([('code', '=', 'WH')])
            warehouse.production_location = production_loc
            warehouse.storage_location = storage_location
            warehouse.save()
            location_sto2 = Location(name='STO 2',
                                     type='storage', code='STO2')
            storage_location.provisioning_location = location_sto2
            # Create chart of accounts
            create_fiscalyear_and_chart(company, None, True)
            accounts = get_accounts(company)
            expense = accounts.get('expense')
            revenue = accounts.get('revenue')
            tax, = Tax.search([], limit=1)
            # Create account category
            account_category = create_account_category(
                tax, expense, revenue, "Servicios")
            account_category_goods = create_account_category(
                tax, expense, revenue)
            # Create products and bom
            template, product = create_product(
                'Product', account_category)
            template.producible = True
            template.purchasable = True
            template.save()
            template2, component1 = create_product(
                'Component 1', account_category_goods, consumable=False)
            template2.producible = True
            template2.purchasable = True
            template2.save()
            template3, component2 = create_product(
                'Component 2', account_category_goods, consumable=True)
            template2.producible = True
            template2.purchasable = True
            template2.save()
            bom = _create_bom(component1, component2, unit)
            # Drag fields from production to request
            work_project = WorkProject(number='OFR001', party=party)
            # Drag fields from production to request
            production1 = create_production(
                3, component1, unit, warehouse, production_loc, company, bom)
            production1.subcontract_product = product
            production1.save()
            # Check project dragging from production to request
            with Transaction().set_context(employee=employee):
                Production.create_purchase_request([production1])
                self.assertIsNotNone(production1.purchase_request)
                self.assertEqual(production1.work_project, production1.purchase_request.project)
            # Check project dragging from requisition to request
            requisition = PurchaseRequisition(supply_date=datetime.date.today() + datetime.timedelta(days=30),
                                              project=work_project, warehouse=warehouse, employee=employee)
            requisition.save()
            requisition_line = PurchaseRequisitionLine(
                product=component1, quantity=Decimal('1.00'), requisition=requisition, supplier=party, unit=unit)
            requisition_line.save()
            PurchaseRequisition.wait([requisition])
            self.assertEqual(requisition.state, 'waiting')
            PurchaseRequisition.approve([requisition])
            self.assertEqual(requisition.state, 'approved')
            PurchaseRequisition.proceed([requisition])
            self.assertEqual(requisition.state, 'processing')
            PurchaseRequisition.process([requisition])
            self.assertEqual(requisition.state, 'processing')
            purchase_request, = PurchaseRequest.search(
                ['origin', '=', 'purchase.requisition.line,' + str(requisition_line.id)])
            self.assertEqual(requisition.project, purchase_request.project)


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(WorkProjectTestCase))
    # suite.addTests(doctest.DocFileSuite(
    #       'scenario_work_project.rst',
    #       setUp=doctest_setup, tearDown=doctest_teardown, encoding='utf-8',
    #       optionflags=doctest.REPORT_ONLY_FIRST_FAILURE,
    #       checker=doctest_checker))
    return suite
